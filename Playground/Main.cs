using Mojio.Events;
using Mojio.TCU.Xirgo.Config;
using Mojio.TCU;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Mojio.Web.Api.Controllers;
using System.Threading;
using Mojio.Web.Api;
using Playground;
using Mojio.TCU.Xirgo;
using Mojio.TCU.Services;

namespace Mojio.TCU.Sandbox
{
    public class MainClass
    {
        
        public enum Env
        {
            DEVELOP,
            SANDBOX,
            STAGING,
            PRODUCTION
        }

        public static Env Environment = Env.PRODUCTION;
        public static Database Database;

        static int Main(string[] args)
        {
            
            switch (Environment)
            {
                case Env.DEVELOP:
                    Database = new Database("mongodb://mojio:13monsters@ds037407.mongolab.com:37407/mojio_develop");
                    break;

                case Env.SANDBOX:
                    Database = new Database("mongodb://sand:TXdJXy9cc9@ds027430-a0.mongolab.com:27430,ds027430-a1.mongolab.com:27430/sandbox");
                    break;

                case Env.STAGING:
                    Database = new Database("mongodb://mojio:13monsters@ds037488.mongolab.com:37488/mojio_staging");
                    break;

                case Env.PRODUCTION:
                    //Database = new Database("mongodb://mojio_production:6Li3eMT4PCtupe5P@ds027430-a0.mongolab.com:27430,ds027430-a1.mongolab.com:27430/production");
                    break;
            }

            try
            {
                
                MojioDbConfig.SetupSchema(Database);
                XConfig.SetDatabase(Database);

                //var c = XConfig.CreateMJP1_02();
                //string s = PacketBuilder.GetXT3040Command(c.Configuration);
                //s = PacketBuilder.GetXT3042Command(c.Configuration);

                string[] firmwareUpdateList = { "358901049077644"};

                string[] configUpdateList = { };

                // Firmware Update
                for (int i = 0; i < firmwareUpdateList.Length; i++ )
                {
                    var mojio = (from m in Database.Get<Mojio>()
                                 where m.Imei == firmwareUpdateList[i]
                                 select m).SingleOrDefault();

                    if (mojio == null)
                    {
                        Console.WriteLine("ERROR: Mojio not found while firmware update. {0}", firmwareUpdateList[i]);
                        break;
                    }
                    else
                    {
                        Console.WriteLine("Updating firmware for {0}", firmwareUpdateList[i]);
                        AbortUpdate(mojio);
                        mojio.DevicePrivate = Database.Get<MojioPrivate>(mojio.Id);
                        UpdateFirmware(mojio);
                    }
                }


                // Configuration Update only
                for (int i = 0; i < configUpdateList.Length; i++)
                {
                    var mojio = (from m in Database.Get<Mojio>()
                                 where m.Imei == configUpdateList[i]
                                 select m).SingleOrDefault();

                    if (mojio == null)
                    {
                        Console.WriteLine("ERROR: Mojio not found while configuration update. {0}", configUpdateList[i]);
                        break;
                    }
                    else
                    {
                        Console.WriteLine("Updating configuration for {0}", configUpdateList[i]);
                        AbortUpdate(mojio);
                        mojio.DevicePrivate = Database.Get<MojioPrivate>(mojio.Id);
                        UpdateFirmware(mojio);

                    }
                }

                Console.ReadLine();

            }
            catch(Exception ex)
            {
                Console.WriteLine(ex);
                Console.ReadLine();
            }
           
            return 0;
        }

        public static void UpdateFirmware(Mojio mojio)
        {
            
            var c = XConfig.CreateMJP1_02();
            MojioUpdate task = new MojioUpdate();
            task.DeviceStamp = c.Configuration.Profile;
            task.MojioId = mojio.Id;
            string resp;
            
            // SMS Reply Number
            var msg = new MojioUpdateMessage();
            msg.Command = PacketBuilder.SetSmsNumber(mojio.Imei, "16042650547", out resp);
            msg.Response = resp;
            task.TaskQueue.Enqueue(msg);

            // FTP Configurations
            msg = new MojioUpdateMessage();
            msg.Command = PacketBuilder.SetFtpServer(mojio.Imei, "54.87.212.233", "mojio", "Macro512", "", 3, out resp);
            msg.Response = resp;
            task.TaskQueue.Enqueue(msg);

            // Issue Firmware Update
            msg = new MojioUpdateMessage();
            msg.Command = PacketBuilder.UpdateFirmware(mojio.Imei, "X0zM-1134BC1-e5", out resp);
            msg.Response = resp;
            task.TaskQueue.Enqueue(msg);

            // Server Configurations
            msg = new MojioUpdateMessage();
            if (mojio.DevicePrivate.Region == "CAN")
                msg.Command = PacketBuilder.SetCommunication(mojio.Imei, 9001, "tcu.moj.io", "", "", "isp.telus.com", "16042650547", 6, out resp);
            else if (mojio.DevicePrivate.Region == "US")
                msg.Command = PacketBuilder.SetCommunication(mojio.Imei, 9001, "tcu.moj.io", "", "", "m2mglobal", "16042650547", 6, out resp);
            else
                throw new ArgumentException("Mojio device does not have a specified region! {0}", mojio.Imei);
            msg.Response = resp;
            msg.HoldOffPeriod = 180;
            task.TaskQueue.Enqueue(msg);

            // Query Firmware Version
            msg = new MojioUpdateMessage();
            msg.Command = PacketBuilder.GetFirmwareVersion(mojio.Imei, out resp, "X0zM-1134BC1-e5");
            msg.Response = resp;
            task.TaskQueue.Enqueue(msg);

            // Power Profile 2
            msg = new MojioUpdateMessage();
            msg.Command = "+XT:5301,2,01,671,0,0,03,0,12.2,13.2,30,15,0,0";
            msg.Response = mojio.Imei + ",5301,2,01,671,00,00,03,00,13.2,13.2,30,15,0,0";
            task.TaskQueue.Enqueue(msg);

            // 3040 configuration
            msg = new MojioUpdateMessage();
            msg.Command = PacketBuilder.GetXT3040Command(c.Configuration);
            msg.Response = PacketBuilder.GetXT3040Response(mojio.Imei, c.Configuration);
            task.TaskQueue.Enqueue(msg);

            // 3042 configuration
            msg = new MojioUpdateMessage();
            msg.Command = PacketBuilder.GetXT3042Command(c.Configuration);
            msg.Response = PacketBuilder.GetXT3042Response(mojio.Imei, c.Configuration);
            task.TaskQueue.Enqueue(msg);

            // Save Profile Name
            msg = new MojioUpdateMessage();
            msg.Command = PacketBuilder.SaveProfile(mojio.Imei, "MjP1_02", out resp);
            msg.Response = resp;
            task.TaskQueue.Enqueue(msg);

            Database.Save(task);

        }

        public static void UpdateConfig(Mojio mojio)
        {
            var c = XConfig.CreateMJP1_02();
            MojioUpdate task = new MojioUpdate();
            task.DeviceStamp = c.Configuration.Profile;
            task.MojioId = mojio.Id;
            string resp;

            var msg = new MojioUpdateMessage();
            msg.Command = PacketBuilder.GetXT3042Command(c.Configuration);
            msg.Response = PacketBuilder.GetXT3042Response(mojio.Imei, c.Configuration);
            task.TaskQueue.Enqueue(msg);

            // 3042 configuration
            msg = new MojioUpdateMessage();
            msg.Command = PacketBuilder.GetXT3042Command(c.Configuration);
            msg.Response = PacketBuilder.GetXT3042Response(mojio.Imei, c.Configuration);
            task.TaskQueue.Enqueue(msg);

            // Save Profile Name
            msg = new MojioUpdateMessage();
            msg.Command = PacketBuilder.SaveProfile(mojio.Imei, "MjP1_02", out resp);
            msg.Response = resp;
            task.TaskQueue.Enqueue(msg);
        }

        public static bool AbortUpdate(Mojio mojio)
        {
            var query = (from u in Database.Get<MojioUpdate>()
                          where u.MojioId == mojio.Id &&
                          u.Complete == false
                          select u).AsQueryable();

            // Abort all tasks in database.
            if ( query != null)
            {
                foreach (var update in query)
                {
                    update.Complete = true;
                    update.Aborted = true;
                    Database.Save(update);
                    Console.WriteLine("Aborting existing update found for {0}.", mojio.Imei);
                }

                return true;
            }

            return false; 
        }


        public static void PoplateMojios()
        {
            System.IO.StreamReader file = new System.IO.StreamReader("c:/temp/update_list.csv");
            System.IO.StreamWriter result = new System.IO.StreamWriter("c:/temp/result_list.csv", true);


            string line;
            while ((line = file.ReadLine()) != null)
            {
                var userQuery = (from u in Database.Get<User>()
                             where u.Email == line
                             select u).SingleOrDefault();

                if (userQuery != null)
                {
                    var mojioQuery = (from m in Database.Get<Mojio>()
                                          where m.OwnerId == userQuery.Id
                                      select m).SingleOrDefault();

                    if (mojioQuery != null)
                    {
                        result.WriteLine(line + "," + mojioQuery.Imei);
                        Console.WriteLine(line + "," + mojioQuery.Imei);
                    }
                       
                    else
                    {
                        result.WriteLine(line + "," + "No Mojio Found");
                        Console.WriteLine(line + "," + "No Mojio Found");
                    }
                        
                }
                else
                {
                       result.WriteLine(line + "," + "No Owner Found");
                       Console.WriteLine(line + "," + "No Owner Found");
                }
            }

            result.Close();
        }
        /// <summary>
        /// Import devices into production!!!
        /// </summary>
        static void ImportDeviceProduction()
        {

            int counter = 0;
            string line;

            // Read the file and display it line by line.
            System.IO.StreamReader file = new System.IO.StreamReader("c:/temp/import_list.csv");
            

            string[] p;
            string[] name = new string[2];
            Mojio mojio;
            MojioPrivate mojioPrivate;
            while ((line = file.ReadLine()) != null)
            {
                p = line.Split(',');
                
                mojio = new Mojio();
                mojio.Imei = p[0];

                var query = (from m in Database.Get<Mojio>()
                             where m.Imei == mojio.Imei
                             select m).SingleOrDefault();

                if (query != null)
                    continue;

                Database.Save(mojio);


                mojioPrivate = new MojioPrivate();
                mojioPrivate.Id = mojio.Id;
                mojioPrivate.PIN = Convert.ToInt32(p[2]);
                mojioPrivate.MSISDN = p[3];
                mojioPrivate.Region = p[4];
                mojioPrivate.LoggerEnabled = true;
                Database.Save(mojioPrivate);
                Console.WriteLine(line);
                counter++;
            }

            file.Close();

            // Suspend the screen.
            Console.ReadLine();
        }

    }


}
