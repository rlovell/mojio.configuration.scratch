using Mojio;
using Mojio.SMS;
using Mojio.TCU.Xirgo;
using Mojio.TCU.Xirgo.Config;
using Mojio.Web.Api;
using Mojio.Web.Api.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Playground
{
    public static class XConfig
    {
        public static Database Database;
        public static void SetDatabase(Database database)
        {
           Database = database;
        }

        public static XirgoConfiguration GetConfiguration(string config)
        {
            MojioConfiguration devConfig = (from c in Database.Get<MojioConfiguration>()
                                             where
                                             c.Id == config &&
                                             c.DeviceType == MojioType.Xirgo
                                             select c).SingleOrDefault();

            if (devConfig == null)
                return null;

            XirgoConfiguration xConfig = new XirgoConfiguration(config);
            xConfig.SetProfile(devConfig);

            return xConfig;
        }

        public static void SaveConfiguration(XirgoConfiguration xConfig)
        {
            MojioConfiguration config = xConfig.Configuration;
            Database.Save(config);
        }


        public static void CreateMJO0_01()
        {
            XirgoConfiguration xConfig = new XirgoConfiguration("MJO0_01");

            // XT3040
            //Ignition On interval
            xConfig.SetConfig(XirgoCode.ONI, 0.5f);

            // Ignition On alert
            xConfig.SetConfig(XirgoCode.ONA, true);

            // Ignition Off interval
            xConfig.SetConfig(XirgoCode.OFI, 10);

            // Ignition Off alert
            xConfig.SetConfig(XirgoCode.OFA, true);

            // Direction Change threshold
            xConfig.SetConfig(XirgoCode.DCT, 30);

            // Speed Threshold
            xConfig.SetConfig(XirgoCode.SPT, 20);

            // RPM Threshold
            xConfig.SetConfig(XirgoCode.RPT, 2500);

            // Mileage Threshold
            xConfig.SetConfig(XirgoCode.MT, 65000);

            // Acceleration Threshold
            xConfig.SetConfig(XirgoCode.AC, 3);

            // Deceleration Threshold
            xConfig.SetConfig(XirgoCode.DC, 3);

            // Battery Threshold
            xConfig.SetConfig(XirgoCode.BT, 11f);

            // GPS power saving mode
            xConfig.SetConfig(XirgoCode.PS, 1);

            // Heartbeat report interval
            xConfig.SetConfig(XirgoCode.PI, 30);

            // Power Up/Reset and GPS lock alert
            xConfig.SetConfig(XirgoCode.PA, true);

            // Set Idle Alert Period
            xConfig.SetConfig(XirgoCode.IDT, 2);

            // Set Tow Alert Enable
            xConfig.SetConfig(XirgoCode.TW, true);

            // Set Tow Start Speed (mph)
            xConfig.SetConfig(XirgoCode.TSTS, 15);

            // Set Tow Stop Speed (mph)
            xConfig.SetConfig(XirgoCode.TSPS, 10);

            // Set Tow Start Time (sec)
            xConfig.SetConfig(XirgoCode.TSTT, 5);

            // Set Tow Stop Time (sec)
            xConfig.SetConfig(XirgoCode.TSPT, 300);

            // Movement Alert Enable
            xConfig.SetConfig(XirgoCode.MS, true);

            // Movement Start Speed
            xConfig.SetConfig(XirgoCode.MSTS, 10);

            // Movement Stop Speed
            xConfig.SetConfig(XirgoCode.MSPS, 5);

            // Movement Start time
            xConfig.SetConfig(XirgoCode.MSTT, 5);

            // Movement Stop time
            xConfig.SetConfig(XirgoCode.MSPT, 10);

            // Park Time Threshold
            xConfig.SetConfig(XirgoCode.PT, 10);

            /*  XT3042  */
            // Accelerometer X Low Threshold
            xConfig.SetConfig(XirgoCode.XL, 462);

            // Accelerometer X High Threshold
            xConfig.SetConfig(XirgoCode.XH, 562); ;

            // Accelerometer Y Low Threshold
            xConfig.SetConfig(XirgoCode.YL, 462);

            // Accelerometer Y High Threshold
            xConfig.SetConfig(XirgoCode.YH, 562);

            // Accelerometer Z Low Threshold
            xConfig.SetConfig(XirgoCode.ZL, 462);

            // Accelerometer Z High Threshold
            xConfig.SetConfig(XirgoCode.ZH, 562);

            // Speed Band Hysteresis
            xConfig.SetConfig(XirgoCode.SBHyst, 15);

            // Speed Band 1 Low
            xConfig.SetConfig(XirgoCode.SB1Begin, 1);

            // Speed Band 1 High
            xConfig.SetConfig(XirgoCode.SB1End, 35);

            // Speed Band 2 Low
            xConfig.SetConfig(XirgoCode.SB2Begin, 36);

            // Speed Band 2 High
            xConfig.SetConfig(XirgoCode.SB2End, 44);

            // Speed Band 3 Low
            xConfig.SetConfig(XirgoCode.SB3Begin, 45);

            // Speed Band 3 High
            xConfig.SetConfig(XirgoCode.SB3End, 60);

            // Speed Band 4 Low
            xConfig.SetConfig(XirgoCode.SB4Begin, 62);

            // Speed Band 4 High
            xConfig.SetConfig(XirgoCode.SB4End, 70);

            // Speed Band 5 Low
            xConfig.SetConfig(XirgoCode.SB5Begin, 75);

            // Speed Band 5 High
            xConfig.SetConfig(XirgoCode.SB5End, 120);

            // Diagnostic Monitoring Enable
            xConfig.SetConfig(XirgoCode.DS, true);

            // DTC Reminder Period
            xConfig.SetConfig(XirgoCode.DRM, 1);

            // Fuel Type
            xConfig.SetConfig(XirgoCode.FT, 0);

            // MPG Type
            xConfig.SetConfig(XirgoCode.MPGT, 0);

            // EFR Coefficient
            xConfig.SetConfig(XirgoCode.EC, 1.00f);

            // MAF Coefficient
            xConfig.SetConfig(XirgoCode.MC, 1.00f);

            // Last Gasp Retries
            xConfig.SetConfig(XirgoCode.LR, 3);

            // Last Gasp Time Between Retries
            xConfig.SetConfig(XirgoCode.LS, 30);

            // SIM Card Change Alert
            xConfig.SetConfig(XirgoCode.SE, true);

            SaveConfiguration(xConfig);
        }

        public static void CreateMJO0_00()
        {
            XirgoConfiguration xConfig = new XirgoConfiguration("MJO0_00");

            // XT3040
            //Ignition On interval
            xConfig.SetConfig(XirgoCode.ONI, 0.5f);

            // Ignition On alert
            xConfig.SetConfig(XirgoCode.ONA, true);

            // Ignition Off interval
            xConfig.SetConfig(XirgoCode.OFI, 10);

            // Ignition Off alert
            xConfig.SetConfig(XirgoCode.OFA, true);

            // Direction Change threshold
            xConfig.SetConfig(XirgoCode.DCT, 30);

            // Speed Threshold
            xConfig.SetConfig(XirgoCode.SPT, 45);

            // RPM Threshold
            xConfig.SetConfig(XirgoCode.RPT, 2500);

            // Mileage Threshold
            xConfig.SetConfig(XirgoCode.MT, 65000);

            // Acceleration Threshold
            xConfig.SetConfig(XirgoCode.AC, 3);

            // Deceleration Threshold
            xConfig.SetConfig(XirgoCode.DC, 3);

            // Battery Threshold
            xConfig.SetConfig(XirgoCode.BT, 11f);

            // GPS power saving mode
            xConfig.SetConfig(XirgoCode.PS, 1);

            // Heartbeat report interval
            xConfig.SetConfig(XirgoCode.PI, 30);

            // Power Up/Reset and GPS lock alert
            xConfig.SetConfig(XirgoCode.PA, true);

            // Set Idle Alert Period
            xConfig.SetConfig(XirgoCode.IDT, 2);

            // Set Tow Alert Enable
            xConfig.SetConfig(XirgoCode.TW, true);

            // Set Tow Start Speed (mph)
            xConfig.SetConfig(XirgoCode.TSTS, 15);

            // Set Tow Stop Speed (mph)
            xConfig.SetConfig(XirgoCode.TSPS, 10);

            // Set Tow Start Time (sec)
            xConfig.SetConfig(XirgoCode.TSTT, 5);

            // Set Tow Stop Time (sec)
            xConfig.SetConfig(XirgoCode.TSPT, 300);

            // Movement Alert Enable
            xConfig.SetConfig(XirgoCode.MS, true);

            // Movement Start Speed
            xConfig.SetConfig(XirgoCode.MSTS, 10);

            // Movement Stop Speed
            xConfig.SetConfig(XirgoCode.MSPS, 5);

            // Movement Start time
            xConfig.SetConfig(XirgoCode.MSTT, 5);

            // Movement Stop time
            xConfig.SetConfig(XirgoCode.MSPT, 10);

            // Park Time Threshold
            xConfig.SetConfig(XirgoCode.PT, 10);

            /*  XT3042  */
            // Accelerometer X Low Threshold
            xConfig.SetConfig(XirgoCode.XL, 462);

            // Accelerometer X High Threshold
            xConfig.SetConfig(XirgoCode.XH, 562); ;

            // Accelerometer Y Low Threshold
            xConfig.SetConfig(XirgoCode.YL, 462);

            // Accelerometer Y High Threshold
            xConfig.SetConfig(XirgoCode.YH, 562);

            // Accelerometer Z Low Threshold
            xConfig.SetConfig(XirgoCode.ZL, 462);

            // Accelerometer Z High Threshold
            xConfig.SetConfig(XirgoCode.ZH, 562);

            // Speed Band Hysteresis
            xConfig.SetConfig(XirgoCode.SBHyst, 15);

            // Speed Band 1 Low
            xConfig.SetConfig(XirgoCode.SB1Begin, 1);

            // Speed Band 1 High
            xConfig.SetConfig(XirgoCode.SB1End, 35);

            // Speed Band 2 Low
            xConfig.SetConfig(XirgoCode.SB2Begin, 36);

            // Speed Band 2 High
            xConfig.SetConfig(XirgoCode.SB2End, 44);

            // Speed Band 3 Low
            xConfig.SetConfig(XirgoCode.SB3Begin, 45);

            // Speed Band 3 High
            xConfig.SetConfig(XirgoCode.SB3End, 60);

            // Speed Band 4 Low
            xConfig.SetConfig(XirgoCode.SB4Begin, 62);

            // Speed Band 4 High
            xConfig.SetConfig(XirgoCode.SB4End, 70);

            // Speed Band 5 Low
            xConfig.SetConfig(XirgoCode.SB5Begin, 75);

            // Speed Band 5 High
            xConfig.SetConfig(XirgoCode.SB5End, 120);

            // Diagnostic Monitoring Enable
            xConfig.SetConfig(XirgoCode.DS, true);

            // DTC Reminder Period
            xConfig.SetConfig(XirgoCode.DRM, 1);

            // Fuel Type
            xConfig.SetConfig(XirgoCode.FT, 0);

            // MPG Type
            xConfig.SetConfig(XirgoCode.MPGT, 0);

            // EFR Coefficient
            xConfig.SetConfig(XirgoCode.EC, 1.00f);

            // MAF Coefficient
            xConfig.SetConfig(XirgoCode.MC, 1.00f);

            // Last Gasp Retries
            xConfig.SetConfig(XirgoCode.LR, 3);

            // Last Gasp Time Between Retries
            xConfig.SetConfig(XirgoCode.LS, 30);

            // SIM Card Change Alert
            xConfig.SetConfig(XirgoCode.SE, true);

            SaveConfiguration(xConfig);
        }

        public static void CreateMJO0_02()
        {
            XirgoConfiguration xConfig = new XirgoConfiguration("MJO0_02");

            // XT3040
            //Ignition On interval
            xConfig.SetConfig(XirgoCode.ONI, 0.5f);

            // Ignition On alert
            xConfig.SetConfig(XirgoCode.ONA, true);

            // Ignition Off interval
            xConfig.SetConfig(XirgoCode.OFI, 10);

            // Ignition Off alert
            xConfig.SetConfig(XirgoCode.OFA, true);

            // Direction Change threshold
            xConfig.SetConfig(XirgoCode.DCT, 45);

            // Speed Threshold
            xConfig.SetConfig(XirgoCode.SPT, 70);

            // RPM Threshold
            xConfig.SetConfig(XirgoCode.RPT, 5000);

            // Mileage Threshold
            xConfig.SetConfig(XirgoCode.MT, 65000);

            // Acceleration Threshold
            xConfig.SetConfig(XirgoCode.AC, 6);

            // Deceleration Threshold
            xConfig.SetConfig(XirgoCode.DC, 6);

            // Battery Threshold
            xConfig.SetConfig(XirgoCode.BT, 11f);

            // GPS power saving mode
            xConfig.SetConfig(XirgoCode.PS, 1);

            // Heartbeat report interval
            xConfig.SetConfig(XirgoCode.PI, 30);

            // Power Up/Reset and GPS lock alert
            xConfig.SetConfig(XirgoCode.PA, true);

            // Set Idle Alert Period
            xConfig.SetConfig(XirgoCode.IDT, 2);

            // Set Tow Alert Enable
            xConfig.SetConfig(XirgoCode.TW, true);

            // Set Tow Start Speed (mph)
            xConfig.SetConfig(XirgoCode.TSTS, 15);

            // Set Tow Stop Speed (mph)
            xConfig.SetConfig(XirgoCode.TSPS, 10);

            // Set Tow Start Time (sec)
            xConfig.SetConfig(XirgoCode.TSTT, 5);

            // Set Tow Stop Time (sec)
            xConfig.SetConfig(XirgoCode.TSPT, 300);

            // Movement Alert Enable
            xConfig.SetConfig(XirgoCode.MS, true);

            // Movement Start Speed
            xConfig.SetConfig(XirgoCode.MSTS, 10);

            // Movement Stop Speed
            xConfig.SetConfig(XirgoCode.MSPS, 5);

            // Movement Start time
            xConfig.SetConfig(XirgoCode.MSTT, 5);

            // Movement Stop time
            xConfig.SetConfig(XirgoCode.MSPT, 10);

            // Park Time Threshold
            xConfig.SetConfig(XirgoCode.PT, 10);

            /*  XT3042  */
            // Accelerometer X Low Threshold
            xConfig.SetConfig(XirgoCode.XL, 462);

            // Accelerometer X High Threshold
            xConfig.SetConfig(XirgoCode.XH, 562); ;

            // Accelerometer Y Low Threshold
            xConfig.SetConfig(XirgoCode.YL, 462);

            // Accelerometer Y High Threshold
            xConfig.SetConfig(XirgoCode.YH, 562);

            // Accelerometer Z Low Threshold
            xConfig.SetConfig(XirgoCode.ZL, 462);

            // Accelerometer Z High Threshold
            xConfig.SetConfig(XirgoCode.ZH, 562);

            // Speed Band Hysteresis
            xConfig.SetConfig(XirgoCode.SBHyst, 15);

            // Speed Band 1 Low
            xConfig.SetConfig(XirgoCode.SB1Begin, 1);

            // Speed Band 1 High
            xConfig.SetConfig(XirgoCode.SB1End, 35);

            // Speed Band 2 Low
            xConfig.SetConfig(XirgoCode.SB2Begin, 36);

            // Speed Band 2 High
            xConfig.SetConfig(XirgoCode.SB2End, 44);

            // Speed Band 3 Low
            xConfig.SetConfig(XirgoCode.SB3Begin, 45);

            // Speed Band 3 High
            xConfig.SetConfig(XirgoCode.SB3End, 60);

            // Speed Band 4 Low
            xConfig.SetConfig(XirgoCode.SB4Begin, 62);

            // Speed Band 4 High
            xConfig.SetConfig(XirgoCode.SB4End, 70);

            // Speed Band 5 Low
            xConfig.SetConfig(XirgoCode.SB5Begin, 75);

            // Speed Band 5 High
            xConfig.SetConfig(XirgoCode.SB5End, 120);

            // Diagnostic Monitoring Enable
            xConfig.SetConfig(XirgoCode.DS, true);

            // DTC Reminder Period
            xConfig.SetConfig(XirgoCode.DRM, 1);

            // Fuel Type
            xConfig.SetConfig(XirgoCode.FT, 0);

            // MPG Type
            xConfig.SetConfig(XirgoCode.MPGT, 0);

            // EFR Coefficient
            xConfig.SetConfig(XirgoCode.EC, 1.00f);

            // MAF Coefficient
            xConfig.SetConfig(XirgoCode.MC, 1.00f);

            // Last Gasp Retries
            xConfig.SetConfig(XirgoCode.LR, 3);

            // Last Gasp Time Between Retries
            xConfig.SetConfig(XirgoCode.LS, 30);

            // SIM Card Change Alert
            xConfig.SetConfig(XirgoCode.SE, true);

            SaveConfiguration(xConfig);
        }

        public static void CreateMJO_LOW()
        {
            XirgoConfiguration xConfig = new XirgoConfiguration("MJO_LOW");

            // XT3040
            //Ignition On interval
            xConfig.SetConfig(XirgoCode.ONI, 60f);

            // Ignition On alert
            xConfig.SetConfig(XirgoCode.ONA, true);

            // Ignition Off interval
            xConfig.SetConfig(XirgoCode.OFI, 120);

            // Ignition Off alert
            xConfig.SetConfig(XirgoCode.OFA, true);

            // Direction Change threshold
            xConfig.SetConfig(XirgoCode.DCT, 0);

            // Speed Threshold
            xConfig.SetConfig(XirgoCode.SPT, 0);

            // RPM Threshold
            xConfig.SetConfig(XirgoCode.RPT, 0);

            // Mileage Threshold
            xConfig.SetConfig(XirgoCode.MT, 65000);

            // Acceleration Threshold
            xConfig.SetConfig(XirgoCode.AC, 0);

            // Deceleration Threshold
            xConfig.SetConfig(XirgoCode.DC, 0);

            // Battery Threshold
            xConfig.SetConfig(XirgoCode.BT, 11f);

            // GPS power saving mode
            xConfig.SetConfig(XirgoCode.PS, 1);

            // Heartbeat report interval
            xConfig.SetConfig(XirgoCode.PI, 0);

            // Power Up/Reset and GPS lock alert
            xConfig.SetConfig(XirgoCode.PA, false);

            // Set Idle Alert Period
            xConfig.SetConfig(XirgoCode.IDT, 0);

            // Set Tow Alert Enable
            xConfig.SetConfig(XirgoCode.TW, true);

            // Set Tow Start Speed (mph)
            xConfig.SetConfig(XirgoCode.TSTS, 0);

            // Set Tow Stop Speed (mph)
            xConfig.SetConfig(XirgoCode.TSPS, 0);

            // Set Tow Start Time (sec)
            xConfig.SetConfig(XirgoCode.TSTT, 0);

            // Set Tow Stop Time (sec)
            xConfig.SetConfig(XirgoCode.TSPT, 0);

            // Movement Alert Enable
            xConfig.SetConfig(XirgoCode.MS, false);

            // Movement Start Speed
            xConfig.SetConfig(XirgoCode.MSTS, 0);

            // Movement Stop Speed
            xConfig.SetConfig(XirgoCode.MSPS, 0);

            // Movement Start time
            xConfig.SetConfig(XirgoCode.MSTT, 0);

            // Movement Stop time
            xConfig.SetConfig(XirgoCode.MSPT, 0);

            // Park Time Threshold
            xConfig.SetConfig(XirgoCode.PT, 0);

            /*  XT3042  */
            // Accelerometer X Low Threshold
            xConfig.SetConfig(XirgoCode.XL, 0);

            // Accelerometer X High Threshold
            xConfig.SetConfig(XirgoCode.XH, 0); ;

            // Accelerometer Y Low Threshold
            xConfig.SetConfig(XirgoCode.YL, 0);

            // Accelerometer Y High Threshold
            xConfig.SetConfig(XirgoCode.YH, 0);

            // Accelerometer Z Low Threshold
            xConfig.SetConfig(XirgoCode.ZL, 0);

            // Accelerometer Z High Threshold
            xConfig.SetConfig(XirgoCode.ZH, 0);

            // Speed Band Hysteresis
            xConfig.SetConfig(XirgoCode.SBHyst, 0);

            // Speed Band 1 Low
            xConfig.SetConfig(XirgoCode.SB1Begin, 0);

            // Speed Band 1 High
            xConfig.SetConfig(XirgoCode.SB1End, 0);

            // Speed Band 2 Low
            xConfig.SetConfig(XirgoCode.SB2Begin, 0);

            // Speed Band 2 High
            xConfig.SetConfig(XirgoCode.SB2End, 0);

            // Speed Band 3 Low
            xConfig.SetConfig(XirgoCode.SB3Begin, 0);

            // Speed Band 3 High
            xConfig.SetConfig(XirgoCode.SB3End, 0);

            // Speed Band 4 Low
            xConfig.SetConfig(XirgoCode.SB4Begin, 0);

            // Speed Band 4 High
            xConfig.SetConfig(XirgoCode.SB4End, 0);

            // Speed Band 5 Low
            xConfig.SetConfig(XirgoCode.SB5Begin, 0);

            // Speed Band 5 High
            xConfig.SetConfig(XirgoCode.SB5End, 0);

            // Diagnostic Monitoring Enable
            xConfig.SetConfig(XirgoCode.DS, false);

            // DTC Reminder Period
            xConfig.SetConfig(XirgoCode.DRM, 0);

            // Fuel Type
            xConfig.SetConfig(XirgoCode.FT, 0);

            // MPG Type
            xConfig.SetConfig(XirgoCode.MPGT, 0);

            // EFR Coefficient
            xConfig.SetConfig(XirgoCode.EC, 1.00f);

            // MAF Coefficient
            xConfig.SetConfig(XirgoCode.MC, 1.00f);

            // Last Gasp Retries
            xConfig.SetConfig(XirgoCode.LR, 0);

            // Last Gasp Time Between Retries
            xConfig.SetConfig(XirgoCode.LS, 30);

            // SIM Card Change Alert
            xConfig.SetConfig(XirgoCode.SE, false);

            SaveConfiguration(xConfig);
        }

        public static XirgoConfiguration CreateMJOb_00()
        {
            XirgoConfiguration xConfig = new XirgoConfiguration("MJOb_00");

            // XT3040
            //Ignition On interval
            xConfig.SetConfig(XirgoCode.ONI, 0.5f);

            // Ignition On alert
            xConfig.SetConfig(XirgoCode.ONA, true);

            // Ignition Off interval
            xConfig.SetConfig(XirgoCode.OFI, 30);

            // Ignition Off alert
            xConfig.SetConfig(XirgoCode.OFA, true);

            // Direction Change threshold
            xConfig.SetConfig(XirgoCode.DCT, 45);

            // Speed Threshold
            xConfig.SetConfig(XirgoCode.SPT, 70);

            // RPM Threshold
            xConfig.SetConfig(XirgoCode.RPT, 5000);

            // Mileage Threshold
            xConfig.SetConfig(XirgoCode.MT, 65000);

            // Acceleration Threshold
            xConfig.SetConfig(XirgoCode.AC, 6);

            // Deceleration Threshold
            xConfig.SetConfig(XirgoCode.DC, 6);

            // Battery Threshold
            xConfig.SetConfig(XirgoCode.BT, 11f);

            // GPS power saving mode
            xConfig.SetConfig(XirgoCode.PS, 1);

            // Heartbeat report interval
            xConfig.SetConfig(XirgoCode.PI, 60);

            // Power Up/Reset and GPS lock alert
            xConfig.SetConfig(XirgoCode.PA, true);

            // Set Idle Alert Period
            xConfig.SetConfig(XirgoCode.IDT, 2);

            // Set Tow Alert Enable
            xConfig.SetConfig(XirgoCode.TW, true);

            // Set Tow Start Speed (mph)
            xConfig.SetConfig(XirgoCode.TSTS, 15);

            // Set Tow Stop Speed (mph)
            xConfig.SetConfig(XirgoCode.TSPS, 10);

            // Set Tow Start Time (sec)
            xConfig.SetConfig(XirgoCode.TSTT, 5);

            // Set Tow Stop Time (sec)
            xConfig.SetConfig(XirgoCode.TSPT, 300);

            // Movement Alert Enable
            xConfig.SetConfig(XirgoCode.MS, true);

            // Movement Start Speed
            xConfig.SetConfig(XirgoCode.MSTS, 10);

            // Movement Stop Speed
            xConfig.SetConfig(XirgoCode.MSPS, 5);

            // Movement Start time
            xConfig.SetConfig(XirgoCode.MSTT, 5);

            // Movement Stop time
            xConfig.SetConfig(XirgoCode.MSPT, 10);

            // Park Time Threshold
            xConfig.SetConfig(XirgoCode.PT, 10);

            /*  XT3042  */
            // Accelerometer X Low Threshold
            xConfig.SetConfig(XirgoCode.XL, 462);

            // Accelerometer X High Threshold
            xConfig.SetConfig(XirgoCode.XH, 562); ;

            // Accelerometer Y Low Threshold
            xConfig.SetConfig(XirgoCode.YL, 462);

            // Accelerometer Y High Threshold
            xConfig.SetConfig(XirgoCode.YH, 562);

            // Accelerometer Z Low Threshold
            xConfig.SetConfig(XirgoCode.ZL, 462);

            // Accelerometer Z High Threshold
            xConfig.SetConfig(XirgoCode.ZH, 562);

            // Speed Band Hysteresis
            xConfig.SetConfig(XirgoCode.SBHyst, 15);

            // Speed Band 1 Low
            xConfig.SetConfig(XirgoCode.SB1Begin, 1);

            // Speed Band 1 High
            xConfig.SetConfig(XirgoCode.SB1End, 35);

            // Speed Band 2 Low
            xConfig.SetConfig(XirgoCode.SB2Begin, 36);

            // Speed Band 2 High
            xConfig.SetConfig(XirgoCode.SB2End, 44);

            // Speed Band 3 Low
            xConfig.SetConfig(XirgoCode.SB3Begin, 45);

            // Speed Band 3 High
            xConfig.SetConfig(XirgoCode.SB3End, 60);

            // Speed Band 4 Low
            xConfig.SetConfig(XirgoCode.SB4Begin, 62);

            // Speed Band 4 High
            xConfig.SetConfig(XirgoCode.SB4End, 70);

            // Speed Band 5 Low
            xConfig.SetConfig(XirgoCode.SB5Begin, 75);

            // Speed Band 5 High
            xConfig.SetConfig(XirgoCode.SB5End, 120);

            // Diagnostic Monitoring Enable
            xConfig.SetConfig(XirgoCode.DS, true);

            // DTC Reminder Period
            xConfig.SetConfig(XirgoCode.DRM, 168);

            // Fuel Type
            xConfig.SetConfig(XirgoCode.FT, 0);

            // MPG Type
            xConfig.SetConfig(XirgoCode.MPGT, 0);

            // EFR Coefficient
            xConfig.SetConfig(XirgoCode.EC, 1.00f);

            // MAF Coefficient
            xConfig.SetConfig(XirgoCode.MC, 1.00f);

            // Last Gasp Retries
            xConfig.SetConfig(XirgoCode.LR, 3);

            // Last Gasp Time Between Retries
            xConfig.SetConfig(XirgoCode.LS, 30);

            // SIM Card Change Alert
            xConfig.SetConfig(XirgoCode.SE, true);

            SaveConfiguration(xConfig);

            return xConfig;
        }

        public static XirgoConfiguration CreateMJOb_01()
        {
            XirgoConfiguration xConfig = new XirgoConfiguration("MJOb_01");

            // XT3040
            //Ignition On interval
            xConfig.SetConfig(XirgoCode.ONI, 0.5f);

            // Ignition On alert
            xConfig.SetConfig(XirgoCode.ONA, true);

            // Ignition Off interval
            xConfig.SetConfig(XirgoCode.OFI, 10);

            // Ignition Off alert
            xConfig.SetConfig(XirgoCode.OFA, true);

            // Direction Change threshold
            xConfig.SetConfig(XirgoCode.DCT, 45);

            // Speed Threshold
            xConfig.SetConfig(XirgoCode.SPT, 70);

            // RPM Threshold
            xConfig.SetConfig(XirgoCode.RPT, 5000);

            // Mileage Threshold
            xConfig.SetConfig(XirgoCode.MT, 65000);

            // Acceleration Threshold
            xConfig.SetConfig(XirgoCode.AC, 6);

            // Deceleration Threshold
            xConfig.SetConfig(XirgoCode.DC, 6);

            // Battery Threshold
            xConfig.SetConfig(XirgoCode.BT, 11f);

            // GPS power saving mode
            xConfig.SetConfig(XirgoCode.PS, 1);

            // Heartbeat report interval
            xConfig.SetConfig(XirgoCode.PI, 60);

            // Power Up/Reset and GPS lock alert
            xConfig.SetConfig(XirgoCode.PA, true);

            // Set Idle Alert Period
            xConfig.SetConfig(XirgoCode.IDT, 2);

            // Set Tow Alert Enable
            xConfig.SetConfig(XirgoCode.TW, true);

            // Set Tow Start Speed (mph)
            xConfig.SetConfig(XirgoCode.TSTS, 15);

            // Set Tow Stop Speed (mph)
            xConfig.SetConfig(XirgoCode.TSPS, 10);

            // Set Tow Start Time (sec)
            xConfig.SetConfig(XirgoCode.TSTT, 5);

            // Set Tow Stop Time (sec)
            xConfig.SetConfig(XirgoCode.TSPT, 300);

            // Movement Alert Enable
            xConfig.SetConfig(XirgoCode.MS, true);

            // Movement Start Speed
            xConfig.SetConfig(XirgoCode.MSTS, 10);

            // Movement Stop Speed
            xConfig.SetConfig(XirgoCode.MSPS, 5);

            // Movement Start time
            xConfig.SetConfig(XirgoCode.MSTT, 5);

            // Movement Stop time
            xConfig.SetConfig(XirgoCode.MSPT, 10);

            // Park Time Threshold
            xConfig.SetConfig(XirgoCode.PT, 10);

            /*  XT3042  */
            // Accelerometer X Low Threshold
            xConfig.SetConfig(XirgoCode.XL, 462);

            // Accelerometer X High Threshold
            xConfig.SetConfig(XirgoCode.XH, 562); ;

            // Accelerometer Y Low Threshold
            xConfig.SetConfig(XirgoCode.YL, 462);

            // Accelerometer Y High Threshold
            xConfig.SetConfig(XirgoCode.YH, 562);

            // Accelerometer Z Low Threshold
            xConfig.SetConfig(XirgoCode.ZL, 462);

            // Accelerometer Z High Threshold
            xConfig.SetConfig(XirgoCode.ZH, 562);

            // Speed Band Hysteresis
            xConfig.SetConfig(XirgoCode.SBHyst, 15);

            // Speed Band 1 Low
            xConfig.SetConfig(XirgoCode.SB1Begin, 1);

            // Speed Band 1 High
            xConfig.SetConfig(XirgoCode.SB1End, 35);

            // Speed Band 2 Low
            xConfig.SetConfig(XirgoCode.SB2Begin, 36);

            // Speed Band 2 High
            xConfig.SetConfig(XirgoCode.SB2End, 44);

            // Speed Band 3 Low
            xConfig.SetConfig(XirgoCode.SB3Begin, 45);

            // Speed Band 3 High
            xConfig.SetConfig(XirgoCode.SB3End, 60);

            // Speed Band 4 Low
            xConfig.SetConfig(XirgoCode.SB4Begin, 62);

            // Speed Band 4 High
            xConfig.SetConfig(XirgoCode.SB4End, 70);

            // Speed Band 5 Low
            xConfig.SetConfig(XirgoCode.SB5Begin, 75);

            // Speed Band 5 High
            xConfig.SetConfig(XirgoCode.SB5End, 120);

            // Diagnostic Monitoring Enable
            xConfig.SetConfig(XirgoCode.DS, true);

            // DTC Reminder Period
            xConfig.SetConfig(XirgoCode.DRM, 168);

            // Fuel Type
            xConfig.SetConfig(XirgoCode.FT, 0);

            // MPG Type
            xConfig.SetConfig(XirgoCode.MPGT, 0);

            // EFR Coefficient
            xConfig.SetConfig(XirgoCode.EC, 1.00f);

            // MAF Coefficient
            xConfig.SetConfig(XirgoCode.MC, 1.00f);

            // Last Gasp Retries
            xConfig.SetConfig(XirgoCode.LR, 3);

            // Last Gasp Time Between Retries
            xConfig.SetConfig(XirgoCode.LS, 30);

            // SIM Card Change Alert
            xConfig.SetConfig(XirgoCode.SE, true);

            SaveConfiguration(xConfig);

            return xConfig;
        }

        public static XirgoConfiguration CreateMJOb_02()
        {
            XirgoConfiguration xConfig = new XirgoConfiguration("MJO1_02");

            // XT3040
            //Ignition On interval
            xConfig.SetConfig(XirgoCode.ONI, 0.5f);

            // Ignition On alert
            xConfig.SetConfig(XirgoCode.ONA, true);

            // Ignition Off interval
            xConfig.SetConfig(XirgoCode.OFI, 10);

            // Ignition Off alert
            xConfig.SetConfig(XirgoCode.OFA, true);

            // Direction Change threshold
            xConfig.SetConfig(XirgoCode.DCT, 30);

            // Speed Threshold
            xConfig.SetConfig(XirgoCode.SPT, 70);

            // RPM Threshold
            xConfig.SetConfig(XirgoCode.RPT, 4000);

            // Mileage Threshold
            xConfig.SetConfig(XirgoCode.MT, 65000);

            // Acceleration Threshold
            xConfig.SetConfig(XirgoCode.AC, 6);

            // Deceleration Threshold
            xConfig.SetConfig(XirgoCode.DC, 6);

            // Battery Threshold
            xConfig.SetConfig(XirgoCode.BT, 11f);

            // GPS power saving mode
            xConfig.SetConfig(XirgoCode.PS, 1);

            // Heartbeat report interval
            xConfig.SetConfig(XirgoCode.PI, 60);

            // Power Up/Reset and GPS lock alert
            xConfig.SetConfig(XirgoCode.PA, true);

            // Set Idle Alert Period
            xConfig.SetConfig(XirgoCode.IDT, 2);

            // Set Tow Alert Enable
            xConfig.SetConfig(XirgoCode.TW, true);

            // Set Tow Start Speed (mph)
            xConfig.SetConfig(XirgoCode.TSTS, 15);

            // Set Tow Stop Speed (mph)
            xConfig.SetConfig(XirgoCode.TSPS, 10);

            // Set Tow Start Time (sec)
            xConfig.SetConfig(XirgoCode.TSTT, 5);

            // Set Tow Stop Time (sec)
            xConfig.SetConfig(XirgoCode.TSPT, 300);

            // Movement Alert Enable
            xConfig.SetConfig(XirgoCode.MS, true);

            // Movement Start Speed
            xConfig.SetConfig(XirgoCode.MSTS, 10);

            // Movement Stop Speed
            xConfig.SetConfig(XirgoCode.MSPS, 5);

            // Movement Start time
            xConfig.SetConfig(XirgoCode.MSTT, 5);

            // Movement Stop time
            xConfig.SetConfig(XirgoCode.MSPT, 10);

            // Park Time Threshold
            xConfig.SetConfig(XirgoCode.PT, 10);

            /*  XT3042  */
            // Accelerometer X Low Threshold
            xConfig.SetConfig(XirgoCode.XL, 462);

            // Accelerometer X High Threshold
            xConfig.SetConfig(XirgoCode.XH, 562); ;

            // Accelerometer Y Low Threshold
            xConfig.SetConfig(XirgoCode.YL, 462);

            // Accelerometer Y High Threshold
            xConfig.SetConfig(XirgoCode.YH, 562);

            // Accelerometer Z Low Threshold
            xConfig.SetConfig(XirgoCode.ZL, 462);

            // Accelerometer Z High Threshold
            xConfig.SetConfig(XirgoCode.ZH, 562);

            // Speed Band Hysteresis
            xConfig.SetConfig(XirgoCode.SBHyst, 15);

            // Speed Band 1 Low
            xConfig.SetConfig(XirgoCode.SB1Begin, 1);

            // Speed Band 1 High
            xConfig.SetConfig(XirgoCode.SB1End, 35);

            // Speed Band 2 Low
            xConfig.SetConfig(XirgoCode.SB2Begin, 36);

            // Speed Band 2 High
            xConfig.SetConfig(XirgoCode.SB2End, 44);

            // Speed Band 3 Low
            xConfig.SetConfig(XirgoCode.SB3Begin, 45);

            // Speed Band 3 High
            xConfig.SetConfig(XirgoCode.SB3End, 60);

            // Speed Band 4 Low
            xConfig.SetConfig(XirgoCode.SB4Begin, 62);

            // Speed Band 4 High
            xConfig.SetConfig(XirgoCode.SB4End, 70);

            // Speed Band 5 Low
            xConfig.SetConfig(XirgoCode.SB5Begin, 75);

            // Speed Band 5 High
            xConfig.SetConfig(XirgoCode.SB5End, 120);

            // Diagnostic Monitoring Enable
            xConfig.SetConfig(XirgoCode.DS, true);

            // DTC Reminder Period
            xConfig.SetConfig(XirgoCode.DRM, 168);

            // Fuel Type
            xConfig.SetConfig(XirgoCode.FT, 0);

            // MPG Type
            xConfig.SetConfig(XirgoCode.MPGT, 0);

            // EFR Coefficient
            xConfig.SetConfig(XirgoCode.EC, 1.00f);

            // MAF Coefficient
            xConfig.SetConfig(XirgoCode.MC, 1.00f);

            // Last Gasp Retries
            xConfig.SetConfig(XirgoCode.LR, 3);

            // Last Gasp Time Between Retries
            xConfig.SetConfig(XirgoCode.LS, 30);

            // SIM Card Change Alert
            xConfig.SetConfig(XirgoCode.SE, true);

            SaveConfiguration(xConfig);

            return xConfig;
        }

        public static XirgoConfiguration CreateMJOb_03()
        {
            XirgoConfiguration xConfig = new XirgoConfiguration("MJOb_03");

            // XT3040
            //Ignition On interval
            xConfig.SetConfig(XirgoCode.ONI, 0.5f);

            // Ignition On alert
            xConfig.SetConfig(XirgoCode.ONA, true);

            // Ignition Off interval
            xConfig.SetConfig(XirgoCode.OFI, 10);

            // Ignition Off alert
            xConfig.SetConfig(XirgoCode.OFA, true);

            // Direction Change threshold
            xConfig.SetConfig(XirgoCode.DCT, 30);

            // Speed Threshold
            xConfig.SetConfig(XirgoCode.SPT, 45);

            // RPM Threshold
            xConfig.SetConfig(XirgoCode.RPT, 4000);

            // Mileage Threshold
            xConfig.SetConfig(XirgoCode.MT, 65000);

            // Acceleration Threshold
            xConfig.SetConfig(XirgoCode.AC, 5);

            // Deceleration Threshold
            xConfig.SetConfig(XirgoCode.DC, 5);

            // Battery Threshold
            xConfig.SetConfig(XirgoCode.BT, 11f);

            // GPS power saving mode
            xConfig.SetConfig(XirgoCode.PS, 1);

            // Heartbeat report interval
            xConfig.SetConfig(XirgoCode.PI, 60);

            // Power Up/Reset and GPS lock alert
            xConfig.SetConfig(XirgoCode.PA, true);

            // Set Idle Alert Period
            xConfig.SetConfig(XirgoCode.IDT, 2);

            // Set Tow Alert Enable
            xConfig.SetConfig(XirgoCode.TW, true);

            // Set Tow Start Speed (mph)
            xConfig.SetConfig(XirgoCode.TSTS, 15);

            // Set Tow Stop Speed (mph)
            xConfig.SetConfig(XirgoCode.TSPS, 10);

            // Set Tow Start Time (sec)
            xConfig.SetConfig(XirgoCode.TSTT, 5);

            // Set Tow Stop Time (sec)
            xConfig.SetConfig(XirgoCode.TSPT, 300);

            // Movement Alert Enable
            xConfig.SetConfig(XirgoCode.MS, true);

            // Movement Start Speed
            xConfig.SetConfig(XirgoCode.MSTS, 10);

            // Movement Stop Speed
            xConfig.SetConfig(XirgoCode.MSPS, 5);

            // Movement Start time
            xConfig.SetConfig(XirgoCode.MSTT, 5);

            // Movement Stop time
            xConfig.SetConfig(XirgoCode.MSPT, 10);

            // Park Time Threshold
            xConfig.SetConfig(XirgoCode.PT, 10);

            /*  XT3042  */
            // Accelerometer X Low Threshold
            xConfig.SetConfig(XirgoCode.XL, 462);

            // Accelerometer X High Threshold
            xConfig.SetConfig(XirgoCode.XH, 562); ;

            // Accelerometer Y Low Threshold
            xConfig.SetConfig(XirgoCode.YL, 462);

            // Accelerometer Y High Threshold
            xConfig.SetConfig(XirgoCode.YH, 562);

            // Accelerometer Z Low Threshold
            xConfig.SetConfig(XirgoCode.ZL, 462);

            // Accelerometer Z High Threshold
            xConfig.SetConfig(XirgoCode.ZH, 562);

            // Speed Band Hysteresis
            xConfig.SetConfig(XirgoCode.SBHyst, 15);

            // Speed Band 1 Low
            xConfig.SetConfig(XirgoCode.SB1Begin, 1);

            // Speed Band 1 High
            xConfig.SetConfig(XirgoCode.SB1End, 35);

            // Speed Band 2 Low
            xConfig.SetConfig(XirgoCode.SB2Begin, 36);

            // Speed Band 2 High
            xConfig.SetConfig(XirgoCode.SB2End, 44);

            // Speed Band 3 Low
            xConfig.SetConfig(XirgoCode.SB3Begin, 45);

            // Speed Band 3 High
            xConfig.SetConfig(XirgoCode.SB3End, 60);

            // Speed Band 4 Low
            xConfig.SetConfig(XirgoCode.SB4Begin, 62);

            // Speed Band 4 High
            xConfig.SetConfig(XirgoCode.SB4End, 70);

            // Speed Band 5 Low
            xConfig.SetConfig(XirgoCode.SB5Begin, 75);

            // Speed Band 5 High
            xConfig.SetConfig(XirgoCode.SB5End, 120);

            // Diagnostic Monitoring Enable
            xConfig.SetConfig(XirgoCode.DS, true);

            // DTC Reminder Period
            xConfig.SetConfig(XirgoCode.DRM, 168);

            // Fuel Type
            xConfig.SetConfig(XirgoCode.FT, 0);

            // MPG Type
            xConfig.SetConfig(XirgoCode.MPGT, 0);

            // EFR Coefficient
            xConfig.SetConfig(XirgoCode.EC, 1.00f);

            // MAF Coefficient
            xConfig.SetConfig(XirgoCode.MC, 1.00f);

            // Last Gasp Retries
            xConfig.SetConfig(XirgoCode.LR, 3);

            // Last Gasp Time Between Retries
            xConfig.SetConfig(XirgoCode.LS, 30);

            // SIM Card Change Alert
            xConfig.SetConfig(XirgoCode.SE, true);

            SaveConfiguration(xConfig);

            return xConfig;
        }

        public static XirgoConfiguration CreateMJOb_04()
        {
            XirgoConfiguration xConfig = new XirgoConfiguration("MJOb_04");

            // XT3040
            //Ignition On interval
            xConfig.SetConfig(XirgoCode.ONI, 0.5f);

            // Ignition On alert
            xConfig.SetConfig(XirgoCode.ONA, true);

            // Ignition Off interval
            xConfig.SetConfig(XirgoCode.OFI, 10);

            // Ignition Off alert
            xConfig.SetConfig(XirgoCode.OFA, true);

            // Direction Change threshold
            xConfig.SetConfig(XirgoCode.DCT, 30);

            // Speed Threshold
            xConfig.SetConfig(XirgoCode.SPT, 70);

            // RPM Threshold
            xConfig.SetConfig(XirgoCode.RPT, 4000);

            // Mileage Threshold
            xConfig.SetConfig(XirgoCode.MT, 65000);

            // Acceleration Threshold
            xConfig.SetConfig(XirgoCode.AC, 3);

            // Deceleration Threshold
            xConfig.SetConfig(XirgoCode.DC, 3);

            // Battery Threshold
            xConfig.SetConfig(XirgoCode.BT, 11f);

            // GPS power saving mode
            xConfig.SetConfig(XirgoCode.PS, 1);

            // Heartbeat report interval
            xConfig.SetConfig(XirgoCode.PI, 60);

            // Power Up/Reset and GPS lock alert
            xConfig.SetConfig(XirgoCode.PA, true);

            // Set Idle Alert Period
            xConfig.SetConfig(XirgoCode.IDT, 2);

            // Set Tow Alert Enable
            xConfig.SetConfig(XirgoCode.TW, true);

            // Set Tow Start Speed (mph)
            xConfig.SetConfig(XirgoCode.TSTS, 15);

            // Set Tow Stop Speed (mph)
            xConfig.SetConfig(XirgoCode.TSPS, 10);

            // Set Tow Start Time (sec)
            xConfig.SetConfig(XirgoCode.TSTT, 5);

            // Set Tow Stop Time (sec)
            xConfig.SetConfig(XirgoCode.TSPT, 300);

            // Movement Alert Enable
            xConfig.SetConfig(XirgoCode.MS, true);

            // Movement Start Speed
            xConfig.SetConfig(XirgoCode.MSTS, 10);

            // Movement Stop Speed
            xConfig.SetConfig(XirgoCode.MSPS, 5);

            // Movement Start time
            xConfig.SetConfig(XirgoCode.MSTT, 5);

            // Movement Stop time
            xConfig.SetConfig(XirgoCode.MSPT, 10);

            // Park Time Threshold
            xConfig.SetConfig(XirgoCode.PT, 10);

            /*  XT3042  */
            // Accelerometer X Low Threshold
            xConfig.SetConfig(XirgoCode.XL, 462);

            // Accelerometer X High Threshold
            xConfig.SetConfig(XirgoCode.XH, 562); ;

            // Accelerometer Y Low Threshold
            xConfig.SetConfig(XirgoCode.YL, 462);

            // Accelerometer Y High Threshold
            xConfig.SetConfig(XirgoCode.YH, 562);

            // Accelerometer Z Low Threshold
            xConfig.SetConfig(XirgoCode.ZL, 462);

            // Accelerometer Z High Threshold
            xConfig.SetConfig(XirgoCode.ZH, 562);

            // Speed Band Hysteresis
            xConfig.SetConfig(XirgoCode.SBHyst, 15);

            // Speed Band 1 Low
            xConfig.SetConfig(XirgoCode.SB1Begin, 1);

            // Speed Band 1 High
            xConfig.SetConfig(XirgoCode.SB1End, 35);

            // Speed Band 2 Low
            xConfig.SetConfig(XirgoCode.SB2Begin, 36);

            // Speed Band 2 High
            xConfig.SetConfig(XirgoCode.SB2End, 44);

            // Speed Band 3 Low
            xConfig.SetConfig(XirgoCode.SB3Begin, 45);

            // Speed Band 3 High
            xConfig.SetConfig(XirgoCode.SB3End, 60);

            // Speed Band 4 Low
            xConfig.SetConfig(XirgoCode.SB4Begin, 62);

            // Speed Band 4 High
            xConfig.SetConfig(XirgoCode.SB4End, 70);

            // Speed Band 5 Low
            xConfig.SetConfig(XirgoCode.SB5Begin, 75);

            // Speed Band 5 High
            xConfig.SetConfig(XirgoCode.SB5End, 120);

            // Diagnostic Monitoring Enable
            xConfig.SetConfig(XirgoCode.DS, true);

            // DTC Reminder Period
            xConfig.SetConfig(XirgoCode.DRM, 168);

            // Fuel Type
            xConfig.SetConfig(XirgoCode.FT, 0);

            // MPG Type
            xConfig.SetConfig(XirgoCode.MPGT, 0);

            // EFR Coefficient
            xConfig.SetConfig(XirgoCode.EC, 1.00f);

            // MAF Coefficient
            xConfig.SetConfig(XirgoCode.MC, 1.00f);

            // Last Gasp Retries
            xConfig.SetConfig(XirgoCode.LR, 3);

            // Last Gasp Time Between Retries
            xConfig.SetConfig(XirgoCode.LS, 30);

            // SIM Card Change Alert
            xConfig.SetConfig(XirgoCode.SE, true);

            SaveConfiguration(xConfig);

            return xConfig;
        }

        public static XirgoConfiguration CreateMJOb_05()
        {
            XirgoConfiguration xConfig = new XirgoConfiguration("MJOb_05");

            // XT3040
            //Ignition On interval
            xConfig.SetConfig(XirgoCode.ONI, 0.5f);

            // Ignition On alert
            xConfig.SetConfig(XirgoCode.ONA, true);

            // Ignition Off interval
            xConfig.SetConfig(XirgoCode.OFI, 60);

            // Ignition Off alert
            xConfig.SetConfig(XirgoCode.OFA, true);

            // Direction Change threshold
            xConfig.SetConfig(XirgoCode.DCT, 30);

            // Speed Threshold
            xConfig.SetConfig(XirgoCode.SPT, 70);

            // RPM Threshold
            xConfig.SetConfig(XirgoCode.RPT, 4000);

            // Mileage Threshold
            xConfig.SetConfig(XirgoCode.MT, 65000);

            // Acceleration Threshold
            xConfig.SetConfig(XirgoCode.AC, 4);

            // Deceleration Threshold
            xConfig.SetConfig(XirgoCode.DC, 4);

            // Battery Threshold
            xConfig.SetConfig(XirgoCode.BT, 11f);

            // GPS power saving mode
            xConfig.SetConfig(XirgoCode.PS, 1);

            // Heartbeat report interval
            xConfig.SetConfig(XirgoCode.PI, 60);

            // Power Up/Reset and GPS lock alert
            xConfig.SetConfig(XirgoCode.PA, true);

            // Set Idle Alert Period
            xConfig.SetConfig(XirgoCode.IDT, 2);

            // Set Tow Alert Enable
            xConfig.SetConfig(XirgoCode.TW, true);

            // Set Tow Start Speed (mph)
            xConfig.SetConfig(XirgoCode.TSTS, 15);

            // Set Tow Stop Speed (mph)
            xConfig.SetConfig(XirgoCode.TSPS, 10);

            // Set Tow Start Time (sec)
            xConfig.SetConfig(XirgoCode.TSTT, 5);

            // Set Tow Stop Time (sec)
            xConfig.SetConfig(XirgoCode.TSPT, 300);

            // Movement Alert Enable
            xConfig.SetConfig(XirgoCode.MS, true);

            // Movement Start Speed
            xConfig.SetConfig(XirgoCode.MSTS, 10);

            // Movement Stop Speed
            xConfig.SetConfig(XirgoCode.MSPS, 5);

            // Movement Start time
            xConfig.SetConfig(XirgoCode.MSTT, 5);

            // Movement Stop time
            xConfig.SetConfig(XirgoCode.MSPT, 10);

            // Park Time Threshold
            xConfig.SetConfig(XirgoCode.PT, 10);

            /*  XT3042  */
            // Accelerometer X Low Threshold
            xConfig.SetConfig(XirgoCode.XL, 462);

            // Accelerometer X High Threshold
            xConfig.SetConfig(XirgoCode.XH, 562); ;

            // Accelerometer Y Low Threshold
            xConfig.SetConfig(XirgoCode.YL, 462);

            // Accelerometer Y High Threshold
            xConfig.SetConfig(XirgoCode.YH, 562);

            // Accelerometer Z Low Threshold
            xConfig.SetConfig(XirgoCode.ZL, 462);

            // Accelerometer Z High Threshold
            xConfig.SetConfig(XirgoCode.ZH, 562);

            // Speed Band Hysteresis
            xConfig.SetConfig(XirgoCode.SBHyst, 15);

            // Speed Band 1 Low
            xConfig.SetConfig(XirgoCode.SB1Begin, 1);

            // Speed Band 1 High
            xConfig.SetConfig(XirgoCode.SB1End, 35);

            // Speed Band 2 Low
            xConfig.SetConfig(XirgoCode.SB2Begin, 36);

            // Speed Band 2 High
            xConfig.SetConfig(XirgoCode.SB2End, 44);

            // Speed Band 3 Low
            xConfig.SetConfig(XirgoCode.SB3Begin, 45);

            // Speed Band 3 High
            xConfig.SetConfig(XirgoCode.SB3End, 60);

            // Speed Band 4 Low
            xConfig.SetConfig(XirgoCode.SB4Begin, 62);

            // Speed Band 4 High
            xConfig.SetConfig(XirgoCode.SB4End, 70);

            // Speed Band 5 Low
            xConfig.SetConfig(XirgoCode.SB5Begin, 75);

            // Speed Band 5 High
            xConfig.SetConfig(XirgoCode.SB5End, 120);

            // Diagnostic Monitoring Enable
            xConfig.SetConfig(XirgoCode.DS, true);

            // DTC Reminder Period
            xConfig.SetConfig(XirgoCode.DRM, 168);

            // Fuel Type
            xConfig.SetConfig(XirgoCode.FT, 0);

            // MPG Type
            xConfig.SetConfig(XirgoCode.MPGT, 0);

            // EFR Coefficient
            xConfig.SetConfig(XirgoCode.EC, 1.00f);

            // MAF Coefficient
            xConfig.SetConfig(XirgoCode.MC, 1.00f);

            // Last Gasp Retries
            xConfig.SetConfig(XirgoCode.LR, 3);

            // Last Gasp Time Between Retries
            xConfig.SetConfig(XirgoCode.LS, 30);

            // SIM Card Change Alert
            xConfig.SetConfig(XirgoCode.SE, true);

            return xConfig;
        }

        public static XirgoConfiguration CreateMJOb_06()
        {
            XirgoConfiguration xConfig = CreateMJOb_05();

            xConfig.Configuration.ProfileId = "MJOb_06";

            // Ignition Off interval
            xConfig.SetConfig(XirgoCode.OFI, 120);

            // Acceleration Threshold
            xConfig.SetConfig(XirgoCode.AC, 5);

            // Deceleration Threshold
            xConfig.SetConfig(XirgoCode.DC, 5);

            // Battery Threshold
            xConfig.SetConfig(XirgoCode.BT, 12.5f);

            SaveConfiguration(xConfig);

            return xConfig;
        }

        public static XirgoConfiguration CreateMJP1_01()
        {
            XirgoConfiguration xConfig = CreateMJOb_06();

            xConfig.Configuration.ProfileId = "Mojio_PROD_01_01";
            xConfig.Configuration.Firmware = "X0zM-1134BC1";

            BmsProfile bms = new BmsProfile();

            bms.Priority.SetValue(2);
            bms.EntryActionMask = (int)(BmsEntryActionMask.GpsOff |
                                  BmsEntryActionMask.ObdOff |
                                  BmsEntryActionMask.CellularOff |
                                  BmsEntryActionMask.SendPreSleepReport |
                                  BmsEntryActionMask.SendLowVoltageAlert);

            bms.EntryCriteriaMask = (int)BmsEntryCriteriaMask.LowVoltage;

            bms.RefreshCriteriaMask = 0;

            bms.RefreshActionMask = 0;

            bms.ExitCriteriaMask = (int)(BmsExitCriteriaMask.ExitAboveVoltage |
                                         BmsExitCriteriaMask.ExitOnPowerCycle |
                                         BmsExitCriteriaMask.ExitWithAlternatorOn);

            bms.ExitActionMask = (int)(BmsExitActionMask.NoAction);

            bms.EntryVoltageLow.SetValue(12.2f);
            bms.ExitVoltageHigh.SetValue(13.2f);
            bms.EntryDebounceTime.SetValue(30);
            bms.ExitDebounceTime.SetValue(15);
            bms.RefreshInterval.SetValue(0);
            bms.RefreshDuration.SetValue(0);

            bms.Description = "Deep sleep dip below 12.2V";

            xConfig.AddBmsProfile(bms);

            string resp;
            string s = PacketBuilder.AddBmsProfile("358901049077644", bms, out resp);

            xConfig.SetConfig(XirgoCode.EPPM, 3);

            SaveConfiguration(xConfig);
            return xConfig;
        }

        public static XirgoConfiguration CreateMJP1_02()
        {
            XirgoConfiguration xConfig = CreateMJP1_01();

            xConfig.Configuration.ProfileId = "MjP1_02";
            xConfig.Configuration.Profile = "Mojio_PROD_01_02";
            xConfig.Configuration.Firmware = "X0zM-1134BC1";

            // Ignition Off interval
            xConfig.SetConfig(XirgoCode.OFI, 60);

            SaveConfiguration(xConfig);
            return xConfig;
        }
        
    }
}
