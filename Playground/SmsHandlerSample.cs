﻿using System;
using SMSClient;

namespace ozeki.net.sms
{
    class SmsHandlerSample
    {
        /// <summary>
        /// ozSMSClient will connect to the NG, and handle communication
        /// </summary>
        private ozSMSClient mySMSClient;

        public SmsHandlerSample()
        {
            //Create SMS client
            mySMSClient = new ozSMSClient();

            //Wire up SMS client events
            mySMSClient.OnClientConnected += mySMSClient_OnClientConnected;
            mySMSClient.OnClientDisconnected += mySMSClient_OnClientDisconnected;
            mySMSClient.OnClientConnectionError += mySMSClient_OnClientConnectionError;
            mySMSClient.OnMessageAcceptedForDelivery += mySMSClient_OnMessageAcceptedForDelivery;
            mySMSClient.OnMessageDeliveredToNetwork += mySMSClient_OnMessageDeliveredToNetwork;
            mySMSClient.OnMessageDeliveredToHandset += mySMSClient_OnMessageDeliveredToHandset;
            mySMSClient.OnMessageDeliveryError += mySMSClient_OnMessageDeliveryError;
            mySMSClient.OnMessageReceived += mySMSClient_OnMessageReceived;
        }

        /// <summary>
        /// Connecting to Ozeki NG SMS Gateway
        /// </summary>
        /// <param name="host">Address of Ozeki NG SMS Gateway</param>
        /// <param name="port">The configured port for NG server</param>
        /// <param name="username">Valid Username for your Ozeki NG SMS Gateway</param>
        /// <param name="password">Valid Password for the given username</param>
        /// <returns>Can or cannot connect to the NG</returns>
        public bool Connect(string host, int port, string username, string password)
        {
            mySMSClient.Username = username;
            mySMSClient.Password = password;
            mySMSClient.Host = host;
            mySMSClient.Port = port;

            mySMSClient.Connected = true;
            if (!mySMSClient.Connected)
                Console.WriteLine(DateTime.Now + " " + "Could not connect to SMS Gateway. Reason: " + mySMSClient.getLastErrorMessage());

            return mySMSClient.Connected;
        }

        /// <summary>
        /// Disconnect from Ozeki NG SMS Gateway
        /// </summary>
        public void Disconnect()
        {
            if (mySMSClient.Connected)
                mySMSClient.Connected = false;
        }

        /// <summary>
        /// Sending SMS through Ozeki NG SMS Gateway
        /// </summary>
        /// <param name="recipient">The (telephone)number of the recipient device</param>
        /// <param name="messageContent">The content of the SMS message</param>
        public void SendSMS(string recipient, string messageContent)
        {
            string msgID = mySMSClient.sendMessage(recipient, messageContent);
            Console.WriteLine(DateTime.Now + " " + "Submitting message to the SMS gateway with ID: " + msgID + "\r\n");
        }


        private void mySMSClient_OnMessageReceived(object sender, DeliveryEventArgs e)
        {
            Console.WriteLine(DateTime.Now + " " + "Message received. Sender address: " + e.Senderaddress + " Message text: " + e.Messagedata + "\r\n");
        }

        private void mySMSClient_OnMessageDeliveryError(object sender, DeliveryErrorEventArgs e)
        {
            Console.WriteLine(DateTime.Now + " " + "Message could not be delivered. ID: " + e.Messageid + " Error message: " + e.ErrorMessage + "\r\n");
        }

        private void mySMSClient_OnMessageDeliveredToHandset(object sender, DeliveryEventArgs e)
        {
            Console.WriteLine(DateTime.Now + " " + "Message delivered to handset. ID: " + e.Messageid + "\r\n");
        }

        private void mySMSClient_OnMessageDeliveredToNetwork(object sender, DeliveryEventArgs e)
        {
            Console.WriteLine(DateTime.Now + " " + "Message delivered to network. ID: " + e.Messageid + "\r\n");
        }

        private void mySMSClient_OnMessageAcceptedForDelivery(object sender, DeliveryEventArgs e)
        {
            Console.WriteLine(DateTime.Now + " " + "Message accepted for delivery. ID: " + e.Messageid + "\r\n");
        }

        private void mySMSClient_OnClientConnectionError(object sender, ErrorEventArgs e)
        {
            Console.WriteLine(DateTime.Now + " " + e.ErrorMessage + "\r\n");
        }

        private void mySMSClient_OnClientDisconnected(object sender, EventArgs e)
        {
            Console.WriteLine(DateTime.Now + " Disconnected from the SMS gateway " + "\r\n");
        }

        private void mySMSClient_OnClientConnected(object sender, EventArgs e)
        {
            Console.WriteLine(DateTime.Now + " Successfully connected to the SMS gateway " + "\r\n");
        }
        
        ~SmsHandlerSample()
        {
            Disconnect();
        }
    }
}
