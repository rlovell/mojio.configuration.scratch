﻿using System;

namespace ozeki.net.sms
{
    class TestSMS
    {
        private static SmsHandlerSample _smsHandlerSample;

        public static void Main(String[] args)
        {
            _smsHandlerSample = new SmsHandlerSample();
            if (!_smsHandlerSample.Connect("localhost", 9500, "admin", "12345")) //Use your settings
                DoExit();

            _smsHandlerSample.SendSMS("123456789", "TestMessage");

            DoExit();
        }

        /// <summary>
        /// Exit from the program
        /// You can receive incoming SMS until the disconnection of the SMS handler
        /// </summary>
        private static void DoExit()
        {
            Console.ReadLine();
            _smsHandlerSample.Disconnect();
            Environment.Exit(0);
        }
    }
}
